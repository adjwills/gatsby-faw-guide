---
title: The FAW Scouting Tool
menuTitle: FAW Scouting Tool
---

<br></br>

We've developed a special tool to help farmers scout their farms for FAW 😊👍

You can access the tool [here](https://faw-scouter.firebaseapp.com/)

<br>

---
if you have any issues please [get in touch with us](https://m.me/afc-dairy)






