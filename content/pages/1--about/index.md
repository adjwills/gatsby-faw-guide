---
title: What is the Fall Army Worm Guide?
menuTitle: What is the FAW Guide?
---

## The importance of educating around the risks of Fall Army Worm

Fall Army Worm has recently come to Africa and can cause billions of dollars of damage to Maize production across the continent.

Those who are affected are smaller scale farmers. That means members of Africa Farmers Club. So we are creating materials to help support members tackling this pest.

This guide is a companion to our chatbot training guide and FAW certification that will enable farmers to get up to speed with Faw. We've developed the FAW expert certification which will help you learn 4 things:

1. How to identify the Fall Army Worm 🐛

2. How to scout your farm effectively 🔍

3. How to decide what to do if you find Fall Army Worm 🌽

4. How you can help other farmers too 👩🏿‍🌾

## Expertise, Top Questions & Real Farmer Experiences!

With the **full subscription** you can unlock **all the topics** in this guide. Each topic includes:

<blockquote style="background: #f2f3f4;">
<img style="height:3em; border: 1px solid; border-color: #ffffff;border-radius: .6em;" src="https://acresofdata.com/wp-content/uploads/2018/04/expert-sources.png" >
<br></br>
☝️ Our guides include research from <b>respected expert sources</b>, you can quickly see this information highlighted light green.
</blockquote>

<blockquote style="background: #f2f3f4;">
<img style="height:3em; border: 1px solid; border-color: #ffffff;border-radius: .6em;" src="https://acresofdata.com/wp-content/uploads/2018/04/top_questions.png" >
<br></br>
☝️ We know that <b>real farmer questions are important</b> so we select top questions from members in blue.
</blockquote>

<blockquote style="background: #f2f3f4;">
<img style="height:3em; border: 1px solid; border-color: #ffffff;border-radius: .6em;" src="https://acresofdata.com/wp-content/uploads/2018/04/answers.png" >
<br></br>
☝️ We then select <b>real farmer answers and farmer experiences</b> that are relevant, you can find these in darker green
</blockquote>

## How do I sign up?

This guide is free for you to use, you can even save it to your phone screen and it will work offline!

