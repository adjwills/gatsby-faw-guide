---
title: How to identify the FAW larvae
subTitle: Identifying the pest
category: "identifying the pest"
cover: id-faw-1-cover.png
---

<blockquote style="background: #f2f3f4;">
<p style="text-align: center; font-weight: 300; font-size: 0.9em">To add your question press <img style="height:2em;" src="https://s3.amazonaws.com/afc-dairytrial/comment_icon.png" >  in bottom corner 👇</p>
</blockquote>

<div style="text-align: center">
<a href="/about"><img style="width: 100%;" src="https://s3.amazonaws.com/afc-dairytrial/key_bar-1.png" ></a>
</div>
<br></br>

Take a look at the pictures below, they will give you some visual clues of the most prominent visual signs of the Fall Army Worm in it's larvae form.

![unsplash.com](./signs_of_faw.png)

<blockquote style="background: #D7F19F;">

### Best features to identify FAW

1. The best identifying feature of the FAW is a set of **four large spots that form a square** on the upper surface of the last segment of its body
2. Larvae tend to conceal themselves during the **brightest time of the day**
3. The face of the mature larva may also be marked with a **white inverted “Y”**
4. the epidermis of the larva is rough or granular in texture when examined closely. However, this larva **does not feel rough to the touch**, as does maize earworm
  <br></br>

  <div>
    Source:
    <img style="height:3.2em; border-radius: 2em; border: 1px solid #ffff" src="https://s3.amazonaws.com/afc-faw-prototype/Feed-the-future-logo.png" >
    <b>Feed the Future</b>
    <div style="float: right;margin-top:0px;"> <b>30</b>
      <a href = "#">
        <img style="height:3.2em; border-radius: 2em;" src="https://s3.amazonaws.com/afc-dairytrial/clap_btn.jpg" >
      </a>
    </div>
  </div>

</blockquote>

### Top Q&A from AFC members

So what are AFC members asking about when it comes to Fall Army Worm?

<blockquote style="background: #6EA6A2;">

  ### John's Question

  Is this the Fall Army Worm?
  <br></br>
    <div>
    <img style="width: 100%" src="https://s3.amazonaws.com/afc-faw-prototype/is_this_the_worm.jpg" >
    </div>
  <br></br>

  <div>
    Posted by:
    <img style="height:3.2em; border-radius: 2em; border: 1px solid #ffff" src="https://s3.amazonaws.com/afc-faw-prototype/John_M.jpg" >
    <b>John M</b>
    <div style="float: right;margin-top:0px;"> <b>12</b>
      <a href = "#">
        <img style="height:3.2em; border-radius: 2em;" src="https://s3.amazonaws.com/afc-dairytrial/clap_btn.jpg" >
      </a>
    </div>
  </div>

</blockquote>

As you can tell from the knowledge you gained from reading the above, John's picture isn't a Fall Army Worm.

Charles from AFC seems to know how to spot what this is...

<blockquote style="background: #96e58e;">

### Charle's Answer

No! It's name is Manduca quiquemaculata (Tomato horn worm)...
<br></br>

  <div>
    Posted by:
    <img style="height:3.2em; border-radius: 2em; border: 1px solid #ffff" src="https://s3.amazonaws.com/afc-faw-prototype/James_k.jpg" >
    <b>Charles A</b>
    <div style="float: right;margin-top:0px;"> <b>31</b>
      <a href = "#">
        <img style="height:3.2em; border-radius: 2em;" src="https://s3.amazonaws.com/afc-dairytrial/clap_btn.jpg" >
      </a>
    </div>
  </div>

</blockquote>

### AFC opinion

We need all farmers to be as PRO at spotting different larvae as Charles. How can we do it?

<blockquote style="background: #D7F19F;">

### Look at more pictures and practice

Practice makes perfect, and the best way to get familiar is to look at lots of pictures and practice with your eyes.
 <br></br>
There are a range of images of the Fall Army Worm below for you to look at, along with some images with are NOT the Fall Army Worm.
  <br></br>

  <div>
    Posted by:
    <img style="height:3.2em; border-radius: 2em; border: 1px solid #ffff" src="https://s3.amazonaws.com/afc-dairytrial/afc_logo.png" >
    <b>AFC</b>
    <div style="float: right;margin-top:0px;"> <b>26</b>
      <a href = "#">
        <img style="height:3.2em; border-radius: 2em;" src="https://s3.amazonaws.com/afc-dairytrial/clap_btn.jpg" >
      </a>
    </div>
  </div>

</blockquote>

### Fall Army Worm Images

So here are some images of the Fall Army Worm for you to get familiar with...

Look out for the distinctive light stripe with side darker stripe below...
![unsplash.com](./example1.jpg)

You can see the upside down Y on the head very clearly in the pic below...
![unsplash.com](./example2.jpg)

Look out for the distinctive 4 dots in the 2nd to last section below, you can also see the 4 dots in a square...

![unsplash.com](./example3.jpg)

The Y shape on the head and the stripe are visible in the image below... 

![unsplash.com](./example4.jpg)

### NOT Fall Army Worm Images

The image below is actually the African Army Worm, notice it has a much darker black stripe...

![unsplash.com](./African-army-worm.jpg)

and this one is a maize stalk borer, you can see it looks different

![unsplash.com](./maize-stalk-borer.jpg)


### The FAW lifecycle

Finally we turn to some more expert advice on the different stages of the Fall Army Worm...

![unsplash.com](./faw_lifecycle.png)

<blockquote style="background: #D7F19F;">

### Learning to identify the pest is critical

The FAW life cycle is completed in about 30 days (at a daily temperature of ~28°C) during the warm summer months but may extend to 60-90 days in cooler temperatures.
<br></br>
  <div>
  <img style="width: 100%" src="https://s3.amazonaws.com/afc-faw-prototype/stages-of-faw.png" >
  </div>
<br></br>
there is an urgent need to generate awareness among farming communities about the life stages of the pest, scouting for the pest (as well as its natural enemies), understanding the right stages of pest control, and implementing low-cost agronomic practices and other landscape management practices for sustainable management of the pest.
<br></br>

  <div>
    Source:
    <img style="height:3.2em; border-radius: 2em; border: 1px solid #ffff" src="https://s3.amazonaws.com/afc-faw-prototype/Feed-the-future-logo.png" >
    <b>Feed the Future</b>
    <div style="float: right;margin-top:0px;"> <b>21</b>
      <a href = "#">
        <img style="height:3.2em; border-radius: 2em;" src="https://s3.amazonaws.com/afc-dairytrial/clap_btn.jpg" >
      </a>
    </div>
  </div>

</blockquote>
Understanding the FAW life cycle is important. Note that the pest will do much greater damage in it's later larvae forms.

![unsplash.com](./eating_through.png)

This means the earlier you can identify the pest the better!

---

### Ready to earn your badge?
Now that you have ready this guide you should be able to test yourself and see if you can earn a badge toward your FAW certification...

Are you ready? Press below to start or click [here]("https://m.me/182930425710385?ref=p1_start")
<br></br>
  <a href="https://m.me/182930425710385?ref=p1_start">
  <img style="width: 100%" src="https://s3.amazonaws.com/afc-faw-prototype/take_test_p1.png" >
  </a>
