---
title: Stages of the Maize cycle
subTitle: Crop scouting
category: "crop scouting"
cover: cover.png
---

<blockquote style="background: #f2f3f4;">
<p style="text-align: center; font-weight: 300; font-size: 0.9em">To add your question press <img style="height:2em;" src="https://s3.amazonaws.com/afc-dairytrial/comment_icon.png" >  in bottom corner 👇</p>
</blockquote>

<div style="text-align: center">
<a href="/about"><img style="width: 100%;" src="https://s3.amazonaws.com/afc-dairytrial/key_bar-1.png" ></a>
</div>
<br></br>

Coming soon...

![unsplash.com](./later.jpg)


---
### Read more topics
There are number of other important topics in this guide:

* [How to identify the FAW larvae](/how-to-identify-the-faw-larvae) ⭐ - Getting to know the basics

...or you can just [see all topics](/)